﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace bbbug.com.util
{
    class netFactory
    {
        static string apiURL = "https://api.bbbug.com/api";
        public string PostFunction(string apiId, string jsonStr)
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072; //一定要有这一句
            Encoding encoding = Encoding.UTF8;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"{apiURL}{apiId}");
            request.Timeout = 5000;
            request.Method = "POST";
            request.Accept = "text/html, application/xhtml+xml, */*";
            request.ContentType = "application/json";
            byte[] buffer = encoding.GetBytes(jsonStr);
            request.ContentLength = buffer.Length;
            request.GetRequestStream().Write(buffer, 0, buffer.Length);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
