﻿using bbbug.com.util;
using MahApps.Metro.Controls.Dialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Windows;

namespace bbbug.com
{
    /// <summary>
    /// login.xaml 的交互逻辑
    /// </summary>
    public partial class login
    {
        public MainWindow mw;
        public login(MainWindow mainWindow)
        {
            InitializeComponent();
            this.mw = mainWindow;
        }

        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            if (mw.token.Equals(""))
            {
                mw.Close();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Post登录接口
            //if(usr.Text == "" || pwd.Password == "")
            if (1 == 2)
            {
                this.ShowMessageAsync("提示","请输入账号&密码!");
            }
            else
            {
                login_BTN.IsEnabled = false;
                netFactory nf = new netFactory();
                //JObject jObject = new JObject { { "plat", "PC" } ,{ "version","1.0" },{ "user_account",usr.Text },{ "user_password",pwd.Password },{ "user_device", "Windows" } };
                JObject jObject = new JObject { { "plat", "PC" }, { "version", "1.0" }, { "user_account", "2625821125@qq.com" }, { "user_password", "W225y924@" }, { "user_device", "Windows" } };
                JObject reJ = (JObject)JsonConvert.DeserializeObject(nf.PostFunction("/user/login", jObject.ToString()));
                if (reJ["code"].ToString().Equals("200"))
                {
                    JObject dataJ = (JObject)reJ["data"];
                    mw.token = dataJ["access_token"].ToString();
                    this.Close();
                    mw.Width = 1024;
                    mw.Height = 768;
                    mw.loadingSource();
                    // mw.Show();
                    mw.Visibility = Visibility.Visible;
                }
                else if (reJ["code"].ToString().Equals("500"))
                {
                    this.ShowMessageAsync("", "账号密码错误!");
                    login_BTN.IsEnabled = true;
                }
                else {
                    login_BTN.IsEnabled = true;
                    this.ShowMessageAsync("", "未整理的错误!!");
                }
            }
           
        }
    }
}
